function BST(value) {
  this.value = value;
  this.left = null;
  this.right = null;
}

BST.prototype.insert = function (value) {
  if (value <= this.value) {
    if (!this.left){ // nodun solunda birşey yok ise...
      this.left = new BST(value); // yeni nodu oraya ekle.
    } else {
      this.left.insert(value); // Recursion : solda nod var ise soldaki noda üzerinde insert metodunu çalıştır.
      // insert metodu her seferinde solda nod olup olmadığını kontrol ediyor ve boş nod bulana kadar
      // soldaki nod üzerinde kendini (insert) yeniden çağırıyor.
    }
  } else if (value > this.value){
    if (!this.right){
      this.right = new BST(value);
    } else {
      this.right.insert(value);
    }
  }
};

BST.prototype.contains = function (searchValue) {
  if (searchValue === this.value) {
    return true;
  } else if (searchValue < this.value){
    if (!this.left){ // nodun solunda başka bir nod yok ise...
      return false;
    } else {
      return this.left.contains(searchValue);  // soldaki nodlar arasında tekrar ara...
    }
  } else if (searchValue > this.value) {
    if (!this.right) {
      return false;
    } else {
      return this.right.contains(searchValue);
    }
  }
};

BST.prototype.depthFirstTraversal = function (iteratorFunc, order) {

  if (order == "pre-order") {
    iteratorFunc(this.value);
  }

  if (this.left) {
    this.left.depthFirstTraversal(iteratorFunc, order);
  }

  if (order == "in-order") {
    iteratorFunc(this.value);
  }

  if (this.right) {
    this.right.depthFirstTraversal(iteratorFunc, order);
  }

  if (order == "post-order") {
    iteratorFunc(this.value);
  }
};

BST.prototype.breadthFirstTraversal = function (iteratorFunc) {
  var queue = [this]; // sıranın başında ilk nodu atıyoruz.

  while(queue.length>0){
    var treeNode = queue.shift(); // sıranın başındaki nodu al
    iteratorFunc(treeNode); // ve noda fonksiyonu uygula
    if (treeNode.left) {
      queue.push(treeNode.left);
    }
    if (treeNode.right){
      queue.push(treeNode.right);
    }
  }
};

// Ağacımızdaki en küçük değer en soldaki değerdir. Dolayısıyla sola doğru en
// uçtaki değeri bulup geri döndürürüz.
BST.prototype.getMinVal = function () {
  if (this.left){
    return this.left.getMinVal()
  } else {
    return this.value;
  }
};

// En büyük değer de en sağdadır...
BST.prototype.getMaxVal = function () {
  if (this.right){
    return this.right.getMaxVal()
  } else {
    return this.value;
  }
};

var bst = new BST(50);
bst.insert(30);
bst.insert(70);
bst.insert(100);
bst.insert(60);
bst.insert(59);
bst.insert(20);
bst.insert(45);
bst.insert(35);
bst.insert(85);
bst.insert(105);
bst.insert(10);

console.log(bst);
console.log(bst.right.left.left); // 59
console.log(bst.right.right); // 100
console.log(bst.left.right.left); // 35

console.log(bst.contains(50));

var logger = function (val) {
  console.log(val);
}

var nodeLogger = function (node) {
  logger(node.value);
}

console.time("in-order");
bst.depthFirstTraversal(logger, "in-order");
console.timeEnd("in-order");

console.time("pre-order");
bst.depthFirstTraversal(logger, "pre-order");
console.timeEnd("pre-order");

console.time("post-order");
bst.depthFirstTraversal(logger, "post-order");
console.timeEnd("post-order");

console.time("breadthFirstTraversal");
bst.breadthFirstTraversal(nodeLogger);
console.timeEnd("breadthFirstTraversal");

console.log("Min", bst.getMinVal());
console.log("Max", bst.getMaxVal());
